/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connection;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import pojos.Programme;
import pojos.RegisteredUsers;
import pojos.StudyCentreCourses;
import pojos.StudyCentres;

/**
 *
 * @author praveen
 */
public class DButil {
    
    private static String msg;
    
    public static <T extends Serializable> boolean insert(Class<T> type, T dbRow) {
        Session session = Controller.getSessionFactory().openSession();
        Transaction transaction = null;
        boolean retVal = true;
        try {
            transaction = session.beginTransaction();
            session.save(dbRow);
            transaction.commit();
        }
        catch(HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
            retVal = false;
        }
        finally {
            session.close();
        }
        return retVal;
    }
    
    public static boolean insertReg(RegisteredUsers user, StudyCentres centre) {
        Session session = Controller.getSessionFactory().openSession();
        Transaction transaction = null;
        boolean retVal = true;
        try {
            transaction = session.beginTransaction();
            centre.setRegisteredUsers(user);
            session.save(centre);
            transaction.commit();
        }
        catch(HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
            retVal = false;
        }
        finally {
            session.close();
        }
        return retVal;
    }
    
    public static void insertCourses(String username, String[] courses) {
    	Session session = Controller.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            String centreCode = session.get(RegisteredUsers.class, username).getStudyCentreCode();
            StudyCentres centre = session.get(StudyCentres.class, centreCode);
            List<StudyCentreCourses> centreCourses = new ArrayList<>();
            int id = 1;
            for (String course : courses) {
            	centreCourses.add(new StudyCentreCourses(id++, 
            			session.get(Programme.class, course.toLowerCase()), 
            			centre));
            }
            for (StudyCentreCourses course : centreCourses) {
            	session.save(course);
            }
            transaction.commit();
        }
        catch(HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
        finally {
            session.close();
        }
    }
    
    public static <T extends Serializable> boolean isKeyInsertable(Class<T> entityType, Serializable id) {
    	System.out.println("session-fac "+Controller.getSessionFactory());
        Session session = Controller.getSessionFactory().openSession();
        if (session.get(entityType, id) == null) {
            session.close();
            return true;
        }
        session.close();
        return false;
    }
    
    public static boolean isUniqueInsertable(String email) {
        Session session = Controller.getSessionFactory().openSession();
        Query query = session.createQuery("select entity.email from RegisteredUsers as entity"
                + " where entity.email = :emailAdd");
        query.setString("emailAdd", email);
        if (query.uniqueResult() == null) {
            session.close();
            return true;
        }
        session.close();
        return false;
    }
    
    public static boolean isValidLogin(String username, String password) {
        Session session = Controller.getSessionFactory().openSession();
        RegisteredUsers user = (RegisteredUsers) session.get(RegisteredUsers.class, username);
        if (user == null || !user.getPassword().equals(password)) {
            session.close();
            return false;
        }
        session.close();
        return true;
    }
}
