package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import connection.DButil;

/**
 * Servlet implementation class CourseRegistrationController
 */
public class CourseRegistrationController extends HttpServlet {

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] courses = request.getParameterValues("courses");
		String username = (String) request.getSession().getAttribute("username");
		String url = "/dashboardBatches.jsp";
		if (courses == null || courses.length == 0) {
			String message="please select atleast one course.";
			request.setAttribute("errorMsg", message);
			url="/dashboard.jsp";
		}
		else {
			DButil.insertCourses(username, courses);
		}
		getServletContext()
        .getRequestDispatcher(url)
        .forward(request, response);
	}

}
