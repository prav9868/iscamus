/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import connection.DButil;
import controller.util.FormValidator;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pojos.RegisteredUsers;
import pojos.StudyCentres;

/**
 *
 * @author praveen
 */
public class RegistrationController extends HttpServlet {


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String email = request.getParameter("email");
        String centreCode = request.getParameter("centreCode");
        String centreName = request.getParameter("centreName");
        String regionalCode = request.getParameter("regionalCode");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String terms = request.getParameter("terms");
        System.out.println("hello world");
        System.out.printf("%s %s %s %s %s %s %s %s %s %n", firstname, lastname, email, username, password, centreCode, centreName, regionalCode, terms);
        RegisteredUsers user = new RegisteredUsers(username, firstname, lastname, email, password, centreCode);
        StudyCentres centre = new StudyCentres(centreName, regionalCode);
        String url = "/index.jsp";
        FormValidator formValidator = new FormValidator();
        if (!formValidator.isValidRegistrationFor(user, centre, terms)
                || !DButil.isKeyInsertable(RegisteredUsers.class, user.getUsername())
                || !DButil.isUniqueInsertable(user.getEmail())) {
            String errorMsg;
            if (!formValidator.isValidRegistrationFor(user, centre, terms)) {
                errorMsg = formValidator.errorMsg();
            }
            else if (!DButil.isKeyInsertable(RegisteredUsers.class, user.getUsername())) {
                errorMsg = "Username allready taken.";
            }
            else {
                errorMsg = "Email address is allready registered";
            }
            request.setAttribute("user", user);
            request.setAttribute("centreName", centreName);
            request.setAttribute("regionalCode", regionalCode);
            request.setAttribute("errorMsg", errorMsg);
            request.setAttribute("register", true);
        }
        else {
            DButil.insertReg(user, centre);
            url = "/thanks.jsp";
        }
        getServletContext()
            .getRequestDispatcher(url)
            .forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Controller for Registering a user.";
    }// </editor-fold>

}
