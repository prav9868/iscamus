package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fif_0026_005ftest;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fc_005fif_0026_005ftest = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.release();
    _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!doctype html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta charset=\"UTF-8\">\n");
      out.write("        <title>IgnouBadhega</title>\n");
      out.write("        <link type=\"text/css\" href=\"css/global.css\" rel=\"stylesheet\">\n");
      out.write("        <link type=\"text/css\" href=\"css/jquery-ui.min.css\" rel=\"stylesheet\">\n");
      out.write("        <script type=\"text/javascript\" src=\"js/jquery-3.3.1.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/global.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/jquery-ui.min.js\"></script>\n");
      out.write("    </head>\n");
      out.write("\n");
      out.write("    <body>\n");
      out.write("        <div class=\"flex-container-column\">\n");
      out.write("            <header class=\"menu\">\n");
      out.write("                <span class=\"mdi mdi-48px mdi-menu\"></span>\n");
      out.write("            </header>\n");
      out.write("            <header class=\"flex-container-row nav-header\">\n");
      out.write("                <a href=\"\" class=\"a-btn flex-container-row site-mark-and-slogan-container\">\n");
      out.write("                    <span class=\"site-mark mdi mdi-school\"></span>\n");
      out.write("                    <span class=\"site-slogan\">Ignou Badhega</span>\n");
      out.write("                </a>\n");
      out.write("                <nav>\n");
      out.write("                    <ul>\n");
      out.write("                        <li class=\"inline-li\"><a href=\"\" class=\"a-btn current\">Home</a></li>\n");
      out.write("                        <li class=\"inline-li\"><a href=\"#services\" class=\"a-btn\">Services</a></li>\n");
      out.write("                        <li class=\"inline-li\"><a href=\"#contact\" class=\"a-btn\">Contact</a></li>\n");
      out.write("                        <li class=\"inline-li\"><a href=\"#feedback\" class=\"a-btn\">Feedback</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </nav>\n");
      out.write("                <div class=\"fill-space-in-between\"></div>\n");
      out.write("                <div class=\"flex-container-row\">\n");
      out.write("                    <a style=\"outline: none;\" href=\"#login-reg\"><span class=\"icon-mark mdi mdi-account\"></span></a>\n");
      out.write("                    <a style=\"outline: none;\" href=\"\"><span class=\"icon-mark mdi mdi-facebook\"></span></a>\n");
      out.write("                    <a href=\"\" class=\"a-btn flex-container-row github\">\n");
      out.write("                        <span  class=\"icon-mark mdi mdi-github-circle\"></span>\n");
      out.write("                        <img class=\"icon-logo\" src=\"img/GitHub_Logo_White.png\">\n");
      out.write("                    </a>\n");
      out.write("                </div>\n");
      out.write("            </header>\n");
      out.write("\n");
      out.write("\n");
      out.write("            <!--site-entrance added for the fixed background image-->\n");
      out.write("            <div class=\"site-entrance\">\n");
      out.write("                <div class=\"flex-container-column welcome\">\n");
      out.write("                    <h1 class=\"wel-content\">Welcome to Ignou Badhega !</h1>\n");
      out.write("                    <div class=\"hr-rule\"></div>\n");
      out.write("                    <h1 class=\"slogan\">Feel free to look around</h1>\n");
      out.write("                    <div class=\"service-btn\"><a class=\"a-btn\" href=\"#services\">G o&nbsp;&nbsp;D i g i t a l</a></div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div id=\"services\" class=\"flex-container-column services\">\n");
      out.write("                <h1>Our Services</h1>\n");
      out.write("                <div class=\"hr-rule\"></div>\n");
      out.write("                <div class=\"flex-container-row service-entries\">\n");
      out.write("                    <div class=\"flex-container-column\">\n");
      out.write("                        <div class=\"service-icon\">\n");
      out.write("                            <span class=\"mdi mdi-48px mdi-database\"></span>\n");
      out.write("                        </div>\n");
      out.write("                        <h2 class=\"service-title\">CRUD Operations</h2>\n");
      out.write("                        <p class=\"service-description\">we facilitate features for all the \n");
      out.write("                            CRUD operations related to the student database. Excel import \n");
      out.write("                            is available as well\n");
      out.write("                        </p>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"flex-container-column\">\n");
      out.write("                        <div class=\"service-icon\">\n");
      out.write("                            <span class=\"mdi mdi-48px mdi-book-open-page-variant\"></span>\n");
      out.write("                        </div>\n");
      out.write("                        <h2 class=\"service-title\">Attendance e-register</h2>\n");
      out.write("                        <p class=\"service-description\">No need to keep the hard copies of the\n");
      out.write("                            attandance of the students. this feature gives a nice digital register\n");
      out.write("                            for the same purpose.\n");
      out.write("                        </p>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"flex-container-column\">\n");
      out.write("                        <div class=\"service-icon\">\n");
      out.write("                            <span class=\"mdi mdi-48px mdi-email\"></span>\n");
      out.write("                        </div>\n");
      out.write("                        <h2 class=\"service-title\">Reminding email's</h2>\n");
      out.write("                        <p class=\"service-description\">Some times the coordinators need to send the reminding emails\n");
      out.write("                            to some or all the students under the centre. this features ease this task by providing\n");
      out.write("                            a nice dashboard feature.\n");
      out.write("                        </p>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"flex-container-column\">\n");
      out.write("                        <div class=\"service-icon\">\n");
      out.write("                            <span class=\"mdi mdi-48px mdi-watch\"></span>\n");
      out.write("                        </div>\n");
      out.write("                        <h2 class=\"service-title\">Generating Time-table</h2>\n");
      out.write("                        <p class=\"service-description\">\n");
      out.write("                            Even though, now a days IGNOU sends all the batch and time table related information to\n");
      out.write("                            the center, still sometime we may need our own generator for the same purpose. so this\n");
      out.write("                            comes as a candy with others feature.\n");
      out.write("                        </p>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <a href=\"#login-reg\" class='a-btn join-btn'>Lets Start By Joining</a>\n");
      out.write("            </div>\n");
      out.write("            \n");
      out.write("            \n");
      out.write("            ");
      if (_jspx_meth_c_005fif_005f0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                \n");
      out.write("            ");
      if (_jspx_meth_c_005fif_005f1(_jspx_page_context))
        return;
      out.write("    \n");
      out.write("            \n");
      out.write("            <div id=\"login-reg\">\n");
      out.write("                <div class=\"left-box-vh flex-container-column\"> \n");
      out.write("                    <form id=\"login\" class=\"flex-container-column\" action=\"login\" method=\"post\">\n");
      out.write("                        <h1>Login</h1>\n");
      out.write("                        <label class=\"form-entry-label\">Username *</label>\n");
      out.write("                        <input type=\"text\" id=\"usernameLogin\" name=\"username\" \n");
      out.write("                               placeholder=\"username\"\n");
      out.write("                               value=\"");
      if (_jspx_meth_c_005fout_005f0(_jspx_page_context))
        return;
      out.write("\">\n");
      out.write("                        <div class=\"focus-blur\">\n");
      out.write("                            <div class=\"static-line\"></div>\n");
      out.write("                            <div class=\"focus-expand\"></div>\n");
      out.write("                        </div>\n");
      out.write("                        <label class=\"form-entry-label\">Password *</label>\n");
      out.write("                        <input type=\"password\" id=\"passwordLogin\" name=\"password\"\n");
      out.write("                               placeholder=\"password\">\n");
      out.write("                        <div class=\"focus-blur\">\n");
      out.write("                            <div class=\"static-line\"></div>\n");
      out.write("                            <div class=\"focus-expand\"></div>\n");
      out.write("                        </div>\n");
      out.write("                        ");
      if (_jspx_meth_c_005fif_005f2(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                        <div id=\"login-btn-container\">\n");
      out.write("                            <button type=\"button\" class=\"login-btn\" id=\"login-button\">Login</button>\n");
      out.write("                        </div>\n");
      out.write("                    </form>\n");
      out.write("                    \n");
      out.write("                    <form id=\"register\" class=\"flex-container-column\" action=\"register\" method=\"post\" novalidate>\n");
      out.write("                        <h1>Register</h1>\n");
      out.write("                        <div id=\"reg-cont\" class=\"flex-container-row\">\n");
      out.write("                            <div class=\"flex-container-column\">\n");
      out.write("                                <label class=\"form-entry-label\">Firstname *</label>\n");
      out.write("                                <input type=\"text\" name=\"firstname\" id=\"firstname\"\n");
      out.write("                                       value=\"");
      if (_jspx_meth_c_005fout_005f2(_jspx_page_context))
        return;
      out.write("\" placeholder=\"firstname\">\n");
      out.write("                                <div class=\"focus-blur\">\n");
      out.write("                                    <div class=\"static-line\"></div>\n");
      out.write("                                    <div class=\"focus-expand\"></div>\n");
      out.write("                                </div>\n");
      out.write("                                <label class=\"form-entry-label\">Lastname *</label>\n");
      out.write("                                <input type=\"text\" name=\"lastname\" id=\"lastname\"\n");
      out.write("                                       value=\"");
      if (_jspx_meth_c_005fout_005f3(_jspx_page_context))
        return;
      out.write("\"\n");
      out.write("                                       placeholder=\"lastname\">\n");
      out.write("                                <div class=\"focus-blur\">\n");
      out.write("                                    <div class=\"static-line\"></div>\n");
      out.write("                                    <div class=\"focus-expand\"></div>\n");
      out.write("                                </div>\n");
      out.write("                                <label class=\"form-entry-label\">Email *</label>\n");
      out.write("                                <input type=\"email\" name=\"email\" id=\"email\"\n");
      out.write("                                       value=\"");
      if (_jspx_meth_c_005fout_005f4(_jspx_page_context))
        return;
      out.write("\"\n");
      out.write("                                       placeholder=\"email address\">\n");
      out.write("                                <div class=\"focus-blur\">\n");
      out.write("                                    <div class=\"static-line\"></div>\n");
      out.write("                                    <div class=\"focus-expand\"></div>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <label class=\"form-entry-label\">Study-Centre Code *</label>\n");
      out.write("                                <input type=\"text\" name=\"centreCode\" id=\"centreCode\"\n");
      out.write("                                       value=\"");
      if (_jspx_meth_c_005fout_005f5(_jspx_page_context))
        return;
      out.write("\"\n");
      out.write("                                       placeholder=\"Centre Code\">\n");
      out.write("                                <div class=\"focus-blur\">\n");
      out.write("                                    <div class=\"static-line\"></div>\n");
      out.write("                                    <div class=\"focus-expand\"></div>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"flex-container-column\">\n");
      out.write("                                <label class=\"form-entry-label\">Study-Centre Name *</label>\n");
      out.write("                                <input type=\"text\" name=\"centreName\" id=\"centreName\"\n");
      out.write("                                       value=\"");
      if (_jspx_meth_c_005fout_005f6(_jspx_page_context))
        return;
      out.write("\"\n");
      out.write("                                       placeholder=\"Centre Name\">\n");
      out.write("                                <div class=\"focus-blur\">\n");
      out.write("                                    <div class=\"static-line\"></div>\n");
      out.write("                                    <div class=\"focus-expand\"></div>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <label class=\"form-entry-label\">Regional-Centre Code *</label>\n");
      out.write("                                <input type=\"text\" name=\"regionalCode\" id=\"regionalCode\"\n");
      out.write("                                       value=\"");
      if (_jspx_meth_c_005fout_005f7(_jspx_page_context))
        return;
      out.write("\"\n");
      out.write("                                       placeholder=\"Regional Centre Code\">\n");
      out.write("                                <div class=\"focus-blur\">\n");
      out.write("                                    <div class=\"static-line\"></div>\n");
      out.write("                                    <div class=\"focus-expand\"></div>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <label class=\"form-entry-label\">Username *</label>\n");
      out.write("                                <input type=\"text\" name=\"username\" id=\"usernameReg\"\n");
      out.write("                                       value=\"");
      if (_jspx_meth_c_005fout_005f8(_jspx_page_context))
        return;
      out.write("\"\n");
      out.write("                                       placeholder=\"username\">\n");
      out.write("                                <div class=\"focus-blur\">\n");
      out.write("                                    <div class=\"static-line\"></div>\n");
      out.write("                                    <div class=\"focus-expand\"></div>\n");
      out.write("                                </div>\n");
      out.write("                                <label class=\"form-entry-label\">Password *</label>\n");
      out.write("                                <input type=\"password\" name=\"password\" id=\"passwordReg\" placeholder=\"password\">\n");
      out.write("                                <div class=\"focus-blur\">\n");
      out.write("                                    <div class=\"static-line\"></div>\n");
      out.write("                                    <div class=\"focus-expand\"></div>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <span id=\"term-cond\" class=\"flex-container-row\">\n");
      out.write("                            <input type=\"checkbox\" name=\"terms\" value=\"accepted\">\n");
      out.write("                            <label class=\"form-entry-label\" style=\"padding: 0 10px;\">I have read the Term & Conditions</label>\n");
      out.write("                        </span>\n");
      out.write("                        ");
      if (_jspx_meth_c_005fif_005f3(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                        <div id=\"register-btn-container\">\n");
      out.write("                            <button type=\"button\" class=\"login-btn\" id=\"register-button\">Register</button>\n");
      out.write("                        </div>\n");
      out.write("                    </form>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"right-box-vh flex-container-column\">\n");
      out.write("                    <h1>New Here?</h1>\n");
      out.write("                    <p>\n");
      out.write("                        Sign up and enjoy digital world of Ignou with\n");
      out.write("                        a bunch of options.\n");
      out.write("                    </p>\n");
      out.write("                    <div>\n");
      out.write("                        <button type=\"button\" id=\"sign-up\" class=\"reg-button\">Sign Up</button>\n");
      out.write("                    </div>\n");
      out.write("                    \n");
      out.write("                    <h1>One Of Us?</h1>\n");
      out.write("                    <p>\n");
      out.write("                        If you already have an account, just sign in.\n");
      out.write("                        We've really missed you!\n");
      out.write("                    </p>\n");
      out.write("                    <div>\n");
      out.write("                        <button type=\"button\" class=\"log-button\">Sign In</button>\n");
      out.write("                    </div>\n");
      out.write("                    \n");
      out.write("                </div>\n");
      out.write("                \n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div id=\"contact\" class=\"tr-box\">\n");
      out.write("                <div class=\"contact flex-container-column\">\n");
      out.write("                    <h1>Need To Talk To Us ?</h1>\n");
      out.write("                    <div class=\"flex-container-row\">\n");
      out.write("                        <div class=\"contact-card flex-container-column\">\n");
      out.write("                            <h2 class=\"designation-title\">Ignou Badhega</h2>\n");
      out.write("                            <div class=\"hr-rule\"></div>\n");
      out.write("                            <div class=\"contact-entry flex-container-row\">\n");
      out.write("                                <span class=\"mdi mdi-36px mdi-phone-in-talk\"></span>\n");
      out.write("                                <p>XXX-XXXXXXX</p>\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"contact-entry flex-container-row\">\n");
      out.write("                                <span class=\"mdi mdi-36px mdi-email\"></span>\n");
      out.write("                                <p>ignoubadhega@xyz.com</p>\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"contact-entry flex-container-row\">\n");
      out.write("                                <span class=\"mdi mdi-36px mdi-office-building\"></span>\n");
      out.write("                                <p>Vikas Puri, Delhi-110018</p>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"contact-card flex-container-column\">\n");
      out.write("                            <h2 class=\"designation-title\">Designer</h2>\n");
      out.write("                            <div class=\"hr-rule\"></div>\n");
      out.write("                            <div class=\"contact-entry flex-container-row\">\n");
      out.write("                                <span class=\"mdi mdi-36px mdi-account-box\"></span>\n");
      out.write("                                <p>Praveen Kumar</p>\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"contact-entry flex-container-row\">\n");
      out.write("                                <span class=\"mdi mdi-36px mdi-email\"></span>\n");
      out.write("                                <p>praveenart.it@gmail.com</p>\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"contact-entry flex-container-row\">\n");
      out.write("                                <span class=\"mdi mdi-36px mdi-linkedin-box\"></span>\n");
      out.write("                                <p>Linked In</p>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div class=\"back-img\">\n");
      out.write("                <div id=\"feedback\" class=\"feedback flex-container-column\">\n");
      out.write("                    <div class=\"flex-container-column\">\n");
      out.write("                        <h1>Feedback</h1>\n");
      out.write("                        <div class=\"hr-rule\"></div>\n");
      out.write("                    </div>\n");
      out.write("                    <form id=\"feedback-form\" class=\"flex-container-row\">\n");
      out.write("                        <div class=\"f-ent-1 flex-container-column\">\n");
      out.write("                            <label class=\"form-entry-label\">Name</label>\n");
      out.write("                            <input type=\"text\" id=\"name\" placeholder=\"Enter your name\">\n");
      out.write("                            <div class=\"focus-blur\">\n");
      out.write("                                <div class=\"static-line\"></div>\n");
      out.write("                                <div class=\"focus-expand\"></div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                        <div class=\"f-ent-2 flex-container-column\">\n");
      out.write("                            <label class=\"form-entry-label\">Email</label>\n");
      out.write("                            <input type=\"email\" id=\"email-id\" placeholder=\"Enter your email address\">\n");
      out.write("                            <div class=\"focus-blur\">\n");
      out.write("                                <div class=\"static-line\"></div>\n");
      out.write("                                <div class=\"focus-expand\"></div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                        <div class=\"f-ent-4 flex-container-column\">\n");
      out.write("                            <label class=\"form-entry-label\">How do you rate your overall experience ?</label>\n");
      out.write("                            <div class=\"flex-container-row\">\n");
      out.write("                                <input type=\"radio\" name=\"msg\"><span class=\"exper-entry\">Bad</span>\n");
      out.write("                                <input type=\"radio\" name=\"msg\"><span class=\"exper-entry\">Average</span>\n");
      out.write("                                <input type=\"radio\" name=\"msg\"><span class=\"exper-entry\">Good</span>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                        <div class=\"f-ent-3 flex-container-column\">\n");
      out.write("                            <label class=\"form-entry-label\">Your Feedback</label>\n");
      out.write("                            <textarea id=\"feed-msg\" placeholder=\"Your text here...\"></textarea>\n");
      out.write("                            <div class=\"focus-blur\">\n");
      out.write("                                <div class=\"static-line\"></div>\n");
      out.write("                                <div class=\"focus-expand\"></div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                        <div class=\"f-ent-5\">\n");
      out.write("                            <button type=\"button\" class=\"def-button\">Submit</button>\n");
      out.write("                        </div>\n");
      out.write("                    </form>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <footer>\n");
      out.write("                <div class=\"flex-container-row\">\n");
      out.write("                    <a href=\"\" id=\"footer-logo\" class=\"a-btn flex-container-row site-mark-and-slogan-container\">\n");
      out.write("                        <span id=\"footer-mark\" class=\"site-mark mdi mdi-school\"></span>\n");
      out.write("                        <span id=\"footer-slogan\" class=\"site-slogan\">Ignou Badhega</span>\n");
      out.write("                    </a>\n");
      out.write("                    <span class=\"designer\">Designed by Praveen Kumar</span>\n");
      out.write("                    <span class=\"license\">\n");
      out.write("                        Code licensed under MIT-style License.\n");
      out.write("                    </span>\n");
      out.write("                </div>\n");
      out.write("            </footer>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_005fif_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_005fif_005f0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_005fif_005f0.setPageContext(_jspx_page_context);
    _jspx_th_c_005fif_005f0.setParent(null);
    // /index.jsp(103,12) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fif_005f0.setTest(false);
    int _jspx_eval_c_005fif_005f0 = _jspx_th_c_005fif_005f0.doStartTag();
    if (_jspx_eval_c_005fif_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                <script>\n");
        out.write("                    $(document).ready(function() {\n");
        out.write("                       $('#sign-up').click();\n");
        out.write("                       document.getElementById(\"login-reg\").scrollIntoView();\n");
        out.write("                    });\n");
        out.write("                </script>\n");
        out.write("            ");
        int evalDoAfterBody = _jspx_th_c_005fif_005f0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_005fif_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.reuse(_jspx_th_c_005fif_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.reuse(_jspx_th_c_005fif_005f0);
    return false;
  }

  private boolean _jspx_meth_c_005fif_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_005fif_005f1 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_005fif_005f1.setPageContext(_jspx_page_context);
    _jspx_th_c_005fif_005f1.setParent(null);
    // /index.jsp(112,12) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fif_005f1.setTest(false);
    int _jspx_eval_c_005fif_005f1 = _jspx_th_c_005fif_005f1.doStartTag();
    if (_jspx_eval_c_005fif_005f1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                <script>\n");
        out.write("                    $(document).ready(function() {\n");
        out.write("                       document.getElementById(\"login-reg\").scrollIntoView();\n");
        out.write("                    });\n");
        out.write("                </script>\n");
        out.write("            ");
        int evalDoAfterBody = _jspx_th_c_005fif_005f1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_005fif_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.reuse(_jspx_th_c_005fif_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.reuse(_jspx_th_c_005fif_005f1);
    return false;
  }

  private boolean _jspx_meth_c_005fout_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_005fout_005f0 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_005fout_005f0.setPageContext(_jspx_page_context);
    _jspx_th_c_005fout_005f0.setParent(null);
    // /index.jsp(127,38) name = value type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fout_005f0.setValue(new String("${username}"));
    int _jspx_eval_c_005fout_005f0 = _jspx_th_c_005fout_005f0.doStartTag();
    if (_jspx_th_c_005fout_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f0);
    return false;
  }

  private boolean _jspx_meth_c_005fif_005f2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_005fif_005f2 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_005fif_005f2.setPageContext(_jspx_page_context);
    _jspx_th_c_005fif_005f2.setParent(null);
    // /index.jsp(139,24) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fif_005f2.setTest(false);
    int _jspx_eval_c_005fif_005f2 = _jspx_th_c_005fif_005f2.doStartTag();
    if (_jspx_eval_c_005fif_005f2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                            <p id=\"login-error\">\n");
        out.write("                                ");
        if (_jspx_meth_c_005fout_005f1(_jspx_th_c_005fif_005f2, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("                            </p>\n");
        out.write("                        ");
        int evalDoAfterBody = _jspx_th_c_005fif_005f2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_005fif_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.reuse(_jspx_th_c_005fif_005f2);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.reuse(_jspx_th_c_005fif_005f2);
    return false;
  }

  private boolean _jspx_meth_c_005fout_005f1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_005fif_005f2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_005fout_005f1 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_005fout_005f1.setPageContext(_jspx_page_context);
    _jspx_th_c_005fout_005f1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_005fif_005f2);
    // /index.jsp(141,32) name = value type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fout_005f1.setValue(new String("${loginError}"));
    int _jspx_eval_c_005fout_005f1 = _jspx_th_c_005fout_005f1.doStartTag();
    if (_jspx_th_c_005fout_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f1);
    return false;
  }

  private boolean _jspx_meth_c_005fout_005f2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_005fout_005f2 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_005fout_005f2.setPageContext(_jspx_page_context);
    _jspx_th_c_005fout_005f2.setParent(null);
    // /index.jsp(155,46) name = value type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fout_005f2.setValue(new String("${user.firstname}"));
    int _jspx_eval_c_005fout_005f2 = _jspx_th_c_005fout_005f2.doStartTag();
    if (_jspx_th_c_005fout_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f2);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f2);
    return false;
  }

  private boolean _jspx_meth_c_005fout_005f3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_005fout_005f3 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_005fout_005f3.setPageContext(_jspx_page_context);
    _jspx_th_c_005fout_005f3.setParent(null);
    // /index.jsp(162,46) name = value type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fout_005f3.setValue(new String("${user.lastname}"));
    int _jspx_eval_c_005fout_005f3 = _jspx_th_c_005fout_005f3.doStartTag();
    if (_jspx_th_c_005fout_005f3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f3);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f3);
    return false;
  }

  private boolean _jspx_meth_c_005fout_005f4(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_005fout_005f4 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_005fout_005f4.setPageContext(_jspx_page_context);
    _jspx_th_c_005fout_005f4.setParent(null);
    // /index.jsp(170,46) name = value type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fout_005f4.setValue(new String("${user.email}"));
    int _jspx_eval_c_005fout_005f4 = _jspx_th_c_005fout_005f4.doStartTag();
    if (_jspx_th_c_005fout_005f4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f4);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f4);
    return false;
  }

  private boolean _jspx_meth_c_005fout_005f5(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_005fout_005f5 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_005fout_005f5.setPageContext(_jspx_page_context);
    _jspx_th_c_005fout_005f5.setParent(null);
    // /index.jsp(179,46) name = value type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fout_005f5.setValue(new String("${user.studyCentreCode}"));
    int _jspx_eval_c_005fout_005f5 = _jspx_th_c_005fout_005f5.doStartTag();
    if (_jspx_th_c_005fout_005f5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f5);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f5);
    return false;
  }

  private boolean _jspx_meth_c_005fout_005f6(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_005fout_005f6 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_005fout_005f6.setPageContext(_jspx_page_context);
    _jspx_th_c_005fout_005f6.setParent(null);
    // /index.jsp(190,46) name = value type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fout_005f6.setValue(new String("${centreName}"));
    int _jspx_eval_c_005fout_005f6 = _jspx_th_c_005fout_005f6.doStartTag();
    if (_jspx_th_c_005fout_005f6.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f6);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f6);
    return false;
  }

  private boolean _jspx_meth_c_005fout_005f7(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_005fout_005f7 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_005fout_005f7.setPageContext(_jspx_page_context);
    _jspx_th_c_005fout_005f7.setParent(null);
    // /index.jsp(199,46) name = value type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fout_005f7.setValue(new String("${regionalCode}"));
    int _jspx_eval_c_005fout_005f7 = _jspx_th_c_005fout_005f7.doStartTag();
    if (_jspx_th_c_005fout_005f7.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f7);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f7);
    return false;
  }

  private boolean _jspx_meth_c_005fout_005f8(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_005fout_005f8 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_005fout_005f8.setPageContext(_jspx_page_context);
    _jspx_th_c_005fout_005f8.setParent(null);
    // /index.jsp(208,46) name = value type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fout_005f8.setValue(new String("${user.username}"));
    int _jspx_eval_c_005fout_005f8 = _jspx_th_c_005fout_005f8.doStartTag();
    if (_jspx_th_c_005fout_005f8.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f8);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f8);
    return false;
  }

  private boolean _jspx_meth_c_005fif_005f3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_005fif_005f3 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_005fif_005f3.setPageContext(_jspx_page_context);
    _jspx_th_c_005fif_005f3.setParent(null);
    // /index.jsp(226,24) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fif_005f3.setTest(false);
    int _jspx_eval_c_005fif_005f3 = _jspx_th_c_005fif_005f3.doStartTag();
    if (_jspx_eval_c_005fif_005f3 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                            <p id=\"reg-error\">\n");
        out.write("                                ");
        if (_jspx_meth_c_005fout_005f9(_jspx_th_c_005fif_005f3, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("                            </p>\n");
        out.write("                        ");
        int evalDoAfterBody = _jspx_th_c_005fif_005f3.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_005fif_005f3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.reuse(_jspx_th_c_005fif_005f3);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.reuse(_jspx_th_c_005fif_005f3);
    return false;
  }

  private boolean _jspx_meth_c_005fout_005f9(javax.servlet.jsp.tagext.JspTag _jspx_th_c_005fif_005f3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_005fout_005f9 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_005fout_005f9.setPageContext(_jspx_page_context);
    _jspx_th_c_005fout_005f9.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_005fif_005f3);
    // /index.jsp(228,32) name = value type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fout_005f9.setValue(new String("${errorMsg}"));
    int _jspx_eval_c_005fout_005f9 = _jspx_th_c_005fout_005f9.doStartTag();
    if (_jspx_th_c_005fout_005f9.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f9);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fout_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005fout_005f9);
    return false;
  }
}
