<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>IgnouBadhega</title>
        <link type="text/css" href="css/global.css" rel="stylesheet">
        <link type="text/css" href="css/jquery-ui.min.css" rel="stylesheet">
        <script type="text/javascript" src="js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="js/global.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    </head>

    <body>
        <div class="flex-container-column">
            <header class="menu">
                <span class="mdi mdi-48px mdi-menu"></span>
            </header>
            <header class="flex-container-row nav-header">
                <a href="" class="a-btn flex-container-row site-mark-and-slogan-container">
                    <span class="site-mark mdi mdi-school"></span>
                    <span class="site-slogan">Ignou Badhega</span>
                </a>
                <nav>
                    <ul>
                        <li class="inline-li"><a href="" class="a-btn current">Home</a></li>
                        <li class="inline-li"><a href="#services" class="a-btn">Services</a></li>
                        <li class="inline-li"><a href="#contact" class="a-btn">Contact</a></li>
                        <li class="inline-li"><a href="#feedback" class="a-btn">Feedback</a></li>
                    </ul>
                </nav>
                <div class="fill-space-in-between"></div>
                <div class="flex-container-row">
                    <a style="outline: none;" href="#login-reg"><span class="icon-mark mdi mdi-account"></span></a>
                    <a style="outline: none;" href=""><span class="icon-mark mdi mdi-facebook"></span></a>
                    <a href="" class="a-btn flex-container-row github">
                        <span  class="icon-mark mdi mdi-github-circle"></span>
                        <img class="icon-logo" src="img/GitHub_Logo_White.png">
                    </a>
                </div>
            </header>


            <!--site-entrance added for the fixed background image-->
            <div class="site-entrance">
                <div class="flex-container-column welcome">
                    <h1 class="wel-content">Welcome to Ignou Badhega !</h1>
                    <div class="hr-rule"></div>
                    <h1 class="slogan">Feel free to look around</h1>
                    <div class="service-btn"><a class="a-btn" href="#services">G o&nbsp;&nbsp;D i g i t a l</a></div>
                </div>
            </div>

            <div id="services" class="flex-container-column services">
                <h1>Our Services</h1>
                <div class="hr-rule"></div>
                <div class="flex-container-row service-entries">
                    <div class="flex-container-column">
                        <div class="service-icon">
                            <span class="mdi mdi-48px mdi-database"></span>
                        </div>
                        <h2 class="service-title">CRUD Operations</h2>
                        <p class="service-description">we facilitate features for all the 
                            CRUD operations related to the student database. Excel import 
                            is available as well
                        </p>
                    </div>
                    <div class="flex-container-column">
                        <div class="service-icon">
                            <span class="mdi mdi-48px mdi-book-open-page-variant"></span>
                        </div>
                        <h2 class="service-title">Attendance e-register</h2>
                        <p class="service-description">No need to keep the hard copies of the
                            attandance of the students. this feature gives a nice digital register
                            for the same purpose.
                        </p>
                    </div>
                    <div class="flex-container-column">
                        <div class="service-icon">
                            <span class="mdi mdi-48px mdi-email"></span>
                        </div>
                        <h2 class="service-title">Reminding email's</h2>
                        <p class="service-description">Some times the coordinators need to send the reminding emails
                            to some or all the students under the centre. this features ease this task by providing
                            a nice dashboard feature.
                        </p>
                    </div>
                    <div class="flex-container-column">
                        <div class="service-icon">
                            <span class="mdi mdi-48px mdi-watch"></span>
                        </div>
                        <h2 class="service-title">Generating Time-table</h2>
                        <p class="service-description">
                            Even though, now a days IGNOU sends all the batch and time table related information to
                            the center, still sometime we may need our own generator for the same purpose. so this
                            comes as a candy with others feature.
                        </p>
                    </div>
                </div>
                <a href="#login-reg" class='a-btn join-btn'>Lets Start By Joining</a>
            </div>
            
            <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <c:if test="${register}">
                <script>
                    $(document).ready(function() {
                       $('#sign-up').click();
                       document.getElementById("login-reg").scrollIntoView();
                    });
                </script>
            </c:if>
                
            <c:if test="${login}">
                <script>
                    $(document).ready(function() {
                       document.getElementById("login-reg").scrollIntoView();
                    });
                </script>
            </c:if>    
            
            <div id="login-reg">
                <div class="left-box-vh flex-container-column"> 
                    <form id="login" class="flex-container-column" action="login" method="post">
                        <h1>Login</h1>
                        <label class="form-entry-label">Username *</label>
                        <input type="text" id="usernameLogin" name="username" 
                               placeholder="username"
                               value="<c:out value='${username}' />">
                        <div class="focus-blur">
                            <div class="static-line"></div>
                            <div class="focus-expand"></div>
                        </div>
                        <label class="form-entry-label">Password *</label>
                        <input type="password" id="passwordLogin" name="password"
                               placeholder="password">
                        <div class="focus-blur">
                            <div class="static-line"></div>
                            <div class="focus-expand"></div>
                        </div>
                        <c:if test="${loginError != ''}">
                            <p id="login-error">
                                <c:out value="${loginError}" />
                            </p>
                        </c:if>
                        <div id="login-btn-container">
                            <button type="button" class="login-btn" id="login-button">Login</button>
                        </div>
                    </form>
                    
                    <form id="register" class="flex-container-column" action="register" method="post" novalidate>
                        <h1>Register</h1>
                        <div id="reg-cont" class="flex-container-row">
                            <div class="flex-container-column">
                                <label class="form-entry-label">Firstname *</label>
                                <input type="text" name="firstname" id="firstname"
                                       value="<c:out value='${user.firstname}' />" placeholder="firstname">
                                <div class="focus-blur">
                                    <div class="static-line"></div>
                                    <div class="focus-expand"></div>
                                </div>
                                <label class="form-entry-label">Lastname *</label>
                                <input type="text" name="lastname" id="lastname"
                                       value="<c:out value='${user.lastname}' />"
                                       placeholder="lastname">
                                <div class="focus-blur">
                                    <div class="static-line"></div>
                                    <div class="focus-expand"></div>
                                </div>
                                <label class="form-entry-label">Email *</label>
                                <input type="email" name="email" id="email"
                                       value="<c:out value='${user.email}' />"
                                       placeholder="email address">
                                <div class="focus-blur">
                                    <div class="static-line"></div>
                                    <div class="focus-expand"></div>
                                </div>

                                <label class="form-entry-label">Study-Centre Code *</label>
                                <input type="text" name="centreCode" id="centreCode"
                                       value="<c:out value='${user.studyCentreCode}' />"
                                       placeholder="Centre Code">
                                <div class="focus-blur">
                                    <div class="static-line"></div>
                                    <div class="focus-expand"></div>
                                </div>
                            </div>

                            <div class="flex-container-column">
                                <label class="form-entry-label">Study-Centre Name *</label>
                                <input type="text" name="centreName" id="centreName"
                                       value="<c:out value='${centreName}' />"
                                       placeholder="Centre Name">
                                <div class="focus-blur">
                                    <div class="static-line"></div>
                                    <div class="focus-expand"></div>
                                </div>

                                <label class="form-entry-label">Regional-Centre Code *</label>
                                <input type="text" name="regionalCode" id="regionalCode"
                                       value="<c:out value='${regionalCode}' />"
                                       placeholder="Regional Centre Code">
                                <div class="focus-blur">
                                    <div class="static-line"></div>
                                    <div class="focus-expand"></div>
                                </div>

                                <label class="form-entry-label">Username *</label>
                                <input type="text" name="username" id="usernameReg"
                                       value="<c:out value='${user.username}' />"
                                       placeholder="username">
                                <div class="focus-blur">
                                    <div class="static-line"></div>
                                    <div class="focus-expand"></div>
                                </div>
                                <label class="form-entry-label">Password *</label>
                                <input type="password" name="password" id="passwordReg" placeholder="password">
                                <div class="focus-blur">
                                    <div class="static-line"></div>
                                    <div class="focus-expand"></div>
                                </div>
                            </div>
                        </div>
                        <span id="term-cond" class="flex-container-row">
                            <input type="checkbox" name="terms" value="accepted">
                            <label class="form-entry-label" style="padding: 0 10px;">I have read the Term & Conditions</label>
                        </span>
                        <c:if test="${errorMsg != ''}">
                            <p id="reg-error">
                                <c:out value="${errorMsg}" />
                            </p>
                        </c:if>
                        <div id="register-btn-container">
                            <button type="button" class="login-btn" id="register-button">Register</button>
                        </div>
                    </form>
                </div>
                <div class="right-box-vh flex-container-column">
                    <h1>New Here?</h1>
                    <p>
                        Sign up and enjoy digital world of Ignou with
                        a bunch of options.
                    </p>
                    <div>
                        <button type="button" id="sign-up" class="reg-button">Sign Up</button>
                    </div>
                    
                    <h1>One Of Us?</h1>
                    <p>
                        If you already have an account, just sign in.
                        We've really missed you!
                    </p>
                    <div>
                        <button type="button" class="log-button">Sign In</button>
                    </div>
                    
                </div>
                
            </div>

            <div id="contact" class="tr-box">
                <div class="contact flex-container-column">
                    <h1>Need To Talk To Us ?</h1>
                    <div class="flex-container-row">
                        <div class="contact-card flex-container-column">
                            <h2 class="designation-title">Ignou Badhega</h2>
                            <div class="hr-rule"></div>
                            <div class="contact-entry flex-container-row">
                                <span class="mdi mdi-36px mdi-phone-in-talk"></span>
                                <p>XXX-XXXXXXX</p>
                            </div>
                            <div class="contact-entry flex-container-row">
                                <span class="mdi mdi-36px mdi-email"></span>
                                <p>ignoubadhega@xyz.com</p>
                            </div>
                            <div class="contact-entry flex-container-row">
                                <span class="mdi mdi-36px mdi-office-building"></span>
                                <p>Vikas Puri, Delhi-110018</p>
                            </div>
                        </div>
                        <div class="contact-card flex-container-column">
                            <h2 class="designation-title">Designer</h2>
                            <div class="hr-rule"></div>
                            <div class="contact-entry flex-container-row">
                                <span class="mdi mdi-36px mdi-account-box"></span>
                                <p>Praveen Kumar</p>
                            </div>
                            <div class="contact-entry flex-container-row">
                                <span class="mdi mdi-36px mdi-email"></span>
                                <p>praveenart.it@gmail.com</p>
                            </div>
                            <div class="contact-entry flex-container-row">
                                <span class="mdi mdi-36px mdi-linkedin-box"></span>
                                <p>Linked In</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="back-img">
                <div id="feedback" class="feedback flex-container-column">
                    <div class="flex-container-column">
                        <h1>Feedback</h1>
                        <div class="hr-rule"></div>
                    </div>
                    <form id="feedback-form" class="flex-container-row">
                        <div class="f-ent-1 flex-container-column">
                            <label class="form-entry-label">Name</label>
                            <input type="text" id="name" placeholder="Enter your name">
                            <div class="focus-blur">
                                <div class="static-line"></div>
                                <div class="focus-expand"></div>
                            </div>
                        </div>

                        <div class="f-ent-2 flex-container-column">
                            <label class="form-entry-label">Email</label>
                            <input type="email" id="email-id" placeholder="Enter your email address">
                            <div class="focus-blur">
                                <div class="static-line"></div>
                                <div class="focus-expand"></div>
                            </div>
                        </div>

                        <div class="f-ent-4 flex-container-column">
                            <label class="form-entry-label">How do you rate your overall experience ?</label>
                            <div class="flex-container-row">
                                <input type="radio" name="msg"><span class="exper-entry">Bad</span>
                                <input type="radio" name="msg"><span class="exper-entry">Average</span>
                                <input type="radio" name="msg"><span class="exper-entry">Good</span>
                            </div>
                        </div>

                        <div class="f-ent-3 flex-container-column">
                            <label class="form-entry-label">Your Feedback</label>
                            <textarea id="feed-msg" placeholder="Your text here..."></textarea>
                            <div class="focus-blur">
                                <div class="static-line"></div>
                                <div class="focus-expand"></div>
                            </div>
                        </div>

                        <div class="f-ent-5">
                            <button type="button" class="def-button">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

            <footer>
                <div class="flex-container-row">
                    <a href="" id="footer-logo" class="a-btn flex-container-row site-mark-and-slogan-container">
                        <span id="footer-mark" class="site-mark mdi mdi-school"></span>
                        <span id="footer-slogan" class="site-slogan">Ignou Badhega</span>
                    </a>
                    <span class="designer">Designed by Praveen Kumar</span>
                    <span class="license">
                        Code licensed under MIT-style License.
                    </span>
                </div>
            </footer>
        </div>
    </body>
</html>